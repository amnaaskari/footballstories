# README #

Implementing sections within embedded video for curating football content. 

### What is this repository for? ###

Two versions currently built with Arsenal videos: 
1. Zuck JS stories 
2. Our own HTML5 implementation 

### Current status ###

This is just a prototype. I'm going to integrate it with canvas so that we have the ability to switch between stories and have a progress bar within stories. Also, the positionings / sizing / icons need to be adjusted.

### How to make any of it work? ###

After cloning the repo, just click on the index.html for whichever one you want to view, launch it with your preferred kind of server and it should work! 

Slack / email me if any confusion !