
/**
 * This function just returns a nice object with the properties built.
 * This could have also been defined itself in the stories array.
 * @param {*} id 
 * @param {*} type 
 * @param {*} length 
 * @param {*} src 
 * @param {*} preview 
 * @param {*} link 
 * @param {*} seen 
 * @param {*} time 
 */
function buildItem(id, type, length, src, preview, link, seen, time) {
    // Using object short-hand (id: id)
      return {
        id,
        type,
        length,
        src,
        preview,
        link,
        seen,
        time,
      };
    }
    
    const stories = new Zuck('stories', {
      backNative: true,
      autoFullScreen: 'false',
      skin: 'Snapgram',
      avatars: 'true',
      list: false,
      cubeEffect: 'true',
      localStorage: true,
      stories: [
        {
          id: 'one',
          photo: './video/arsenal/logo.png',
          name: 'FA Cup Draw',
          link: '',
          lastUpdated: 1492665454,
          items: [
          buildItem('1', 'video', 0, './video/arsenal/facupdraw.mp4', '', '', false, 1492665454),
          ],
        },
        {
          id: 'two',
          photo: './video/arsenal/logo.png',
          name: 'Title Hopes',
          link: '',
          lastUpdated: 1492665454,
          items: [
            buildItem('2', 'video', 0, './video/arsenal/titlehopes.mp4', './video/arsenal/logo.png', '', false, 1492665454),
          ],
        },
        {
          id: 'three',
          photo: './video/arsenal/logo.png',
          name: 'Did you miss?',
          link: '',
          lastUpdated: 1492665454,
          items: [buildItem('2', 'video', 0, './video/arsenal/didumiss.mp4', './video/arsenal/logo.png', '', false, 1492665454),
           ],  
        },
        {
          id: 'four',
          photo: './video/arsenal/logo.png',
          name: 'Thursdays game',
          link: '',
          lastUpdated: 1492665454,
          items: [
            buildItem('4', 'video', 0, './video/arsenal/thursdaysgame.mp4', './video/arsenal/logo.png', '', false, 1492665454),
            
         ],  
        }
      ],
    });

          