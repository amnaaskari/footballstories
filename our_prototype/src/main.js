var stories = document.querySelectorAll(".videoblock"),
videos = document.querySelectorAll("video"),
closes = document.querySelectorAll(".close"),
mutes = document.querySelectorAll(".mute");
substoryControls = document.querySelectorAll(".progressblock")
navigation = document.querySelector(".navigationblock"); 

// we could add a name property to each item in the substory objects.. just so that its more clear 

storyData= [{1:{start:0, end:5.2},
    2: {start:5, end: 9},
    3: {start:9, end:13},
    4: {start:13, end: 17},
    5: {start:17, end: 21}},
   {1:{start:0, end:7}},
   {1:{start:0, end:3},
    2:{start:3, end:7}}, 
   {1:{start:0, end:3},
    2:{start:3, end:5}},
   {1:{start:0, end:1},
    2:{start:1, end:3},
    3:{start:3, end:7}}
   ];

var sources = [];

videos.forEach(function (video){sources.push(video.children[0].src)})
stories.forEach(function (story){story.addEventListener("click",activateStory);}) 
//// 
document.onwebkitfullscreenchange = function ifNotfullscreen ()  {
    if(!document.fullscreenElement && !document.webkitIsFullScreen && !document.mozFullScreen && !document.msFullscreenElement) {
        videos.forEach(function (video){video.pause();video.parentElement.children.item(3).setAttribute('data-state','hidden');
        video.parentElement.children.item(4).setAttribute('data-state','hidden');   navigation.setAttribute('data-state','hidden')
        video.currentTime=0;}) // this means, when we're out of full screen or we exit full screen, video is paused
    }
}
/////
substoryControls.forEach(function (substoryController){substoryController.addEventListener("click",activateController)}) 
closes.forEach(function (close){close.addEventListener("click",exit);})
mutes.forEach(function (mute){mute.addEventListener("click",goMute);})

function activateStory(s) {

    if (s.target != s.currentTarget) {
        if (s.target.className==="play" || s.target.tagName==="VIDEO") {
            
        launchStory(this)                 
        }
    }
}

function launchStory (video_block) {
    
    video_block.children.item(0).setAttribute('data-state','hidden') //making the play buttons invisible 
    var video = video_block.children.item(1);
    playPause(video) 

 
}
function playPause(someVideo) {
    if(someVideo.paused) {
        someVideo.play();
        someVideo.webkitRequestFullscreen();
        someVideo.parentElement.children.item(3).setAttribute('data-state','visible') //showing close button
        someVideo.parentElement.children.item(4).setAttribute('data-state','visible')
        navigation.setAttribute('data-state','visible')            
        
    }
    else {
        someVideo.pause();
    }
    var story_index = someVideo.parentElement.getAttribute("data-index")
    var story = storyData[story_index]
    var progressbars = someVideo.nextElementSibling.children;
    operateProgressbars(); 
    
    function operateProgressbars (){
        someVideo.addEventListener("timeupdate", function () {
                var time = someVideo.currentTime;
                if (time<story[1].end && time>story[1].start && '1' in story){
                    progressbars[0].value = (time-story[1].start) / story[1].end; 
                } 
                else if('2' in story && time<story[2].end && time>story[2].start) {
                    progressbars[1].value = (time-story[2].start) / (story[2].end-story[2].start);
                }
                else if('3' in story && time<story[3].end && time>story[3].start ) {
                    progressbars[2].value = (time-story[3].start) / (story[3].end-story[3].start);
                }
                else if('4' in story && time<story[4].end && time>story[4].start) {
                    progressbars[3].value = (time-story[4].start) / (story[4].end-story[4].start);
                }
                else if('5' in story && time<story[5].end && time>story[5].start ) {
                    progressbars[4].value = (time-story[5].start) / (story[5].end-story[5].start);
                }
            
        } )
    }
}




//////

var clickCount_next = 0,
clickCount_previous = 0; 
var next = document.querySelector(".next"),
previous = document.querySelector(".previous");

next.addEventListener("click", function () { 
    clickCount_next++
    var current_video = document.webkitFullscreenElement, 
    index = parseInt(current_video.parentElement.getAttribute("data-index"));

    if (clickCount_next >= sources.length - index) {
        clickCount_next = 0; 
        // simulate pressing the cross button 
    }

    if(sources[index+clickCount_next] !== undefined) {
        current_video.src = sources[index+clickCount_next]
        index = index+clickCount_next;
        current_video.setAttribute("data-index",index)
    }


    playPause(document.webkitFullscreenElement)
    

    
})

previous.addEventListener("click", function () { 
    clickCount_previous++
    
    var current_video = document.webkitFullscreenElement; 
    index = parseInt(current_video.getAttribute("data-index"));
    
    if (index===0){
        document.webkitExitFullscreen(); 
    }
    if (clickCount_previous > index) {
        clickCount_previous = 0; 
    }

    if(sources[index-clickCount_previous] !== undefined) {
        current_video.src = sources[index-clickCount_previous]
        index = index-clickCount_previous;
    }  
    playPause(document.webkitFullscreenElement)
})


///////

function exit () {
    document.webkitCancelFullScreen(); 
     var original_index = parseInt(this.parentElement.getAttribute("data-index"))
    this.parentElement.children.item(1).src = sources[original_index]; 
    clickCount_next = 0; 
    // console.log("updated index" + original_index)
}

function goMute() {
    if (!this.parentElement.children.item(1).muted) {
        this.style.backgroundImage = "url(vid/volume.png)"
        
        this.parentElement.children.item(1).muted = true; 
    }
    else {
        this.style.backgroundImage = "url(vid/mute.png)"
        this.parentElement.children.item(1).muted = false; 
    }
}

var increments = Object.values(Array.from(new Array(1000),(val,index)=>index));; 
            
function activateController (c) {
    if (c.target != c.currentTarget) {
        var story_index = parseInt(c.target.parentElement.parentElement.getAttribute("data-index"))
        var story = storyData[story_index]; 
        var progress_bar = c.target; 
        var video = c.target.parentElement.parentElement.children.item(1);
        // console.log(c.target.className)
      switch (c.target.className) {
        case "substory_1": if ('1' in story){if(video.paused){video.play()};video.currentTime= story[1].start; updateValue(progress_bar,(story[1].end-story[1].start),video)}; break; 
        case "substory_2": if ('2' in story){if(video.paused){video.play()};video.currentTime= story[2].start; updateValue(progress_bar,(story[2].end-story[2].start),video)}; break; 
        case "substory_3": if ('3' in story){if(video.paused){video.play()};video.currentTime= story[3].start; updateValue(progress_bar,(story[3].end-story[3].start),video)}; break; 
        case "substory_4": if ('4' in story){if(video.paused){video.play()};video.currentTime= story[4].start; updateValue(progress_bar,(story[4].end-story[4].start),video)}; break;
        case "substory_5": if ('5' in story){if(video.paused){video.play()};video.currentTime= story[5].start; updateValue(progress_bar,(story[5].end-story[5].start),video)}; break;
     // however many there are 
      }
    }
}


function updateValue (progress_bar,video_length,video) {

    increments.forEach(function(increment, index){
        setTimeout(function(){if(!video.paused){progress_bar.value+=0.001};},video_length*index)
    })
    progress_bar.value =0 ; // so that it restarts  
}
 

